from django.shortcuts import render #, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views import generic
from django.urls import reverse
import json

from .forms import LoginForm, RegisterForm

from .models import User, Question, Quiz, Response, Class, Record

#---------------------------------------------
#-----------------------------Utility Functions
#----------------------------------------------


#Handles whether the user is logged in or has permission to log in to the page
#0 - Everyone
#1 - Student
#2 - TA
#3 - Teacher
#plevel represents the permission level required to access site, uid represents uid of user coming in (-1 represents someone who isn't logged in)
def handlePermissions(plevel, uid):
    permission = 0
    if uid != -1:
        try:
            permission = User.objects.get(pk=uid).permissions
        except User.DoesNotExist:
            print("Something went wrong in HandlePermissions - You passed a uid that does not exist in the database")
    #If the user does not have permission to access this site
    if permission < plevel:
        #Redirect the user back to login page
        return False
    return True

def getUser(uid):
    #Declare u
    u
    try:
        u = User.objects.get(pk=uid)
    except User.DoesNotExist:
        u = None
        #When we try to get a user that does not exist
    return u

def getUserId(uid):
    try:
        u = User.objects.get(pk=uid)
        return u.pk
    except User.DoesNotExist:
        return -1 

def order_questions(quiz):
    questions = Question.objects.filter(quiz=quiz).order_by('order', 'pk')
    amt = -1
    for i in range(len(questions)):
        questions[i].order = i
        questions[i].save()
        amt = i
    return amt + 1

#----------------------------------------------
#------------------------------------Ajax Views
#----------------------------------------------

#Function to return true if a username is already in use
def validate_user_ajax(request):
    info = request.POST.get('info', None)
    validate_type = request.POST.get('type', None)
    is_taken = False
    #If we are checking the username
    if(validate_type == 'name'):
        is_taken = User.objects.filter(name=info).exists()
    #If we are checking email addresses
    if(validate_type == 'email'):
        is_taken = User.objects.filter(email=info).exists()

    data = {
        'is_taken': is_taken
    }
    return JsonResponse(data)

#Function to handle adding a question
#NOT FINISHED
def modify_question_ajax(request):
    if request.method == 'POST':
        #TODO - validate uid to make sure that the person has permission to access this
        #TODO - ensure that there doesn't exist an identical question for the same quiz
        content = {}
        try:
            content = json.loads(request.body)
        except:
            content = request.POST

        data = {'success': False}

        #If we're adding a question
        if content['type'] == "add_question":
            quiz = Quiz.objects.get(pk=int(content['quizId']))
            o = order_questions(quiz)
            #Create the question using data from the user
            q = Question(
                text=content['question'],
                quiz=Quiz.objects.get(pk=int(content['quizId'])),
                order = o,
            )
            #Save it in the database
            q.save()
            #Send the id back to the page so that the page can create the correct elements
            data['id'] = q.pk
            data['success'] = True
        elif content['type'] == "delete_question":
            #Get the question that the person is referring to
            q = Question.objects.get(pk=int(content['questionId']))
            #Delete it
            q.delete()
            #Tell the page that we succeeded
            data['success'] = True
        elif content['type'] == "edit_question":
            #Get the question that the person is referring to
            q = Question.objects.get(pk=int(content['questionId']))
            #Edit the name
            q.text = content['newQuestion']
            q.save()
            #Tell the page that we succeeded
            data['success'] = True
        elif content['type'] == "set_correct_response":
            #Get the question that the person is referring to
            q = Question.objects.get(pk=int(content['questionId']))
            #Edit the correct response
            q.correct_response = Response.objects.get(pk=content['responseId'])
            q.save()
            #Tell the page that we succeeded
            data['success'] = True
        elif content['type'] == "swap_question":
            o = order_questions(Quiz.objects.get(pk=int(content['quizId'])))
            q1 = Question.objects.get(pk=int(content['firstQuestion']))
            q2 = Question.objects.get(pk=int(content['secondQuestion']))
            temp = q1.order
            q1.order = q2.order
            q2.order = temp
            q1.save()
            q2.save()
            data['success'] = True
        else:
            print("Unrecognized type in modify_question_ajax")
            print(content)
        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('index'))
    
def modify_response_ajax(request):
    if request.method == 'POST':
        #TODO - validate uid to make sure that the person has permission to access this
        #TODO - ensure that there doesn't exist an identical response for the same question

        content = {}
        try:
            content = json.loads(request.body)
        except:
            content = request.POST

        data = {'success': False}

        #If we're adding a response
        if content['type'] == "add_response":
            #Create the response using data from the user
            r = Response(
                text=content['response'],
                question=Question.objects.get(pk=int(content['questionId']))
            )
            #Save it in the database
            r.save()
            #Send the id back to the page so that the page can create the correct elements
            data['id'] = r.pk
            data['success'] = True
        elif content['type'] == "delete_response":
            #Get the response that the person is referring to
            r = Response.objects.get(pk=int(content['responseId']))
            #Delete it
            r.delete()
            #Tell the page that we succeeded
            data['success'] = True
        elif content['type'] == "edit_response":
            #Get the response that the person is referring to
            r = Response.objects.get(pk=int(content['responseId']))
            #Edit the name
            r.text = content['newResponse']
            r.save()
            #Tell the page that we succeeded
            data['success'] = True
        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('index'))

def publish_quiz_ajax(request):
    if request.method == "POST":
        #TODO - validate uid and everything to make sure we can publish this quiz
        data = {'success': False}
        q = Quiz.objects.get(pk=int(request.POST.get('quizId', None)))
        if request.POST.get('type', None) == 'publish':
            q.published = True
            data['success'] = True
        else:
            print("Error: Invalid input to publish_quiz_ajax type")

        q.save()
        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('index'))

#Ajax function to add a new quiz, edit a quiz, or delete a quiz
def modify_quiz_ajax(request):
    if request.method == 'POST':
        #TODO - validate uid to make sure that the person has permission to access this

        data = {'success': False}

        #If we're adding a quiz
        if request.POST.get('type', None) == "add_quiz":
            #Create the quiz using data from the user
            #TODO - when class model is up and running, make sure that this gets changed
            q = Quiz(
                name=request.POST.get('name', None),
                class_for=Class.objects.get(pk=int(request.POST.get('class_for', None))),
                author=User.objects.get(pk=int(request.POST.get('uid', None))),
            )
            #Save it in the database
            q.save()
            #Send the id back to the page so that the page can create the correct elements
            data['id'] = q.pk
            data['success'] = True
        elif request.POST.get('type', None) == "copy_quiz":
            #TODO - when class model is up and running, make sure that this gets changed
            quizToCopy = Quiz.objects.get(pk=int(request.POST.get('quizId', None)))
            q = Quiz(
                name=quizToCopy.name + " copy",
                class_for=quizToCopy.class_for,
                author = quizToCopy.author
            )
            #Save it in the database
            q.save()
            #Copy old questions over
            for oldQuestion in Question.objects.filter(quiz=quizToCopy):
                newQuestion = Question(
                    text=oldQuestion.text,
                    quiz=q
                )
                newQuestion.save()
                #Copy old responses over
                for oldResponse in Response.objects.filter(question=oldQuestion):
                    newResponse = Response(
                        text=oldResponse.text,
                        question=newQuestion
                    )
                    newResponse.save()
                #Link the correct response
                if oldQuestion.correct_response is not None:
                    newQuestion.correct_response = Response.objects.get(question=newQuestion, text=oldQuestion.correct_response.text)
                    

            #Send the id back to the page so that the page can create the correct elements
            data['id'] = q.pk
            data['name'] = q.name
            data['url'] = reverse('edit_quiz', kwargs={'quiz_id': q.pk})
            data['success'] = True
        elif request.POST.get('type', None) == "delete_quiz":
            #Get the quiz that the person is referring to
            q = Quiz.objects.get(pk=int(request.POST.get('quizId', None)))
            #Delete it
            q.delete()
            #Tell the page that we succeeded
            data['success'] = True
        elif request.POST.get('type', None) == "edit_quiz":
            #TODO - expand this as needed to edit more things that the user could want to edit
            #Get the quiz that the person is referring to
            q = Response.objects.get(pk=int(request.POST.get('quizId', None)))
            #Edit the name
            q.text = request.POST.get('quizName', None)
            q.save()
            #Tell the page that we succeeded
            data['success'] = True
        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('index'))

def modify_class_ajax(request):
    if request.method == 'POST':
        data = {}
        data['success'] = False
        if request.POST.get('type', None) == 'add_student':
            c = Class.objects.get(pk=int(request.POST.get('classId', None)))
            student = User.objects.get(pk=int(request.POST.get('studentId', None)))
            student.classes.add(c)
            student.save()
            data['success'] = True
        elif request.POST.get('type', None) == 'remove_student':
            c = Class.objects.get(pk=int(request.POST.get('classId', None)))
            student = User.objects.get(pk=int(request.POST.get('studentId', None)))
            student.classes.remove(c)
            student.save()
            data['success'] = True

        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('index'))

#Function to return remaining questions and responses for a live quiz
def get_quiz_info_ajax(request):
    if request.method == 'POST':
        content = json.loads(request.body)
        quiz = Quiz.objects.get(pk=content['quiz_id'])
        questionList = []
        for q in Question.objects.filter(quiz=quiz, order__gt=quiz.current_question).order_by('order'):
            responses = [[r.pk, r.text] for r in Response.objects.filter(question=q)]
            question = [q.pk, q.text, q.correct_response.pk, responses]
            questionList = questionList + [question]
        return JsonResponse({ 'question_list': questionList })
    else:
        return HttpResponseRedirect(reverse('index'))

#----------------------------------------------
#------------------------------Form submissions
#----------------------------------------------

#View to handle a user registering
def register_form(request):
    #If the user is attempting to register
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            doesNotExist = False
            #If the name that they entered isn't in use by anyone and the password isn't either
            if User.objects.filter(name=form.cleaned_data['name']).count() == 0: #and User.objects.filter(email=form.cleaned_data['email']).count() == 0:
                doesNotExist = True

            #If the user entered valid registration information
            if doesNotExist:
                #Then we create a new user account with their information
                u = User(
                    name=form.cleaned_data['name'],
                    #password=form.cleaned_data['password'],
                    permissions=1,
                    #email=form.cleaned_data['email']
                )
                u.save()
                u.classes.add(1)
                #added this to redirect to the quiz after registration
                if u != None:
                    # This is where we set a cookie to say that the user is logged in
                    request.session['uid'] = u.pk
                    # return HttpResponseRedirect(reverse("index"))
                    return HttpResponseRedirect(reverse("quizlist"))

                #Finally, redirect them to the login page where they can use their information
                #return HttpResponseRedirect(reverse('login') + "?message=Registration+Successful")
            else:
                #If someone is using data that is already in use by someone else
                return HttpResponseRedirect(reverse('register') + "?message=Someone+already+has+that+information")
        else:
            #If someone does not have valid form data somehow
            return HttpResponseRedirect(reverse('register') + "?message=Invalid+form+data")
    else:
        #If someone is accessing the page without a post request
        return HttpResponseRedirect(reverse('index') + "?message=Looks+like+hacking")


#----------------------------------------------
#--------------------------------Template Views
#----------------------------------------------

def index(request): 
    #Open to everyone
    if not handlePermissions(0, getUserId(request.session.get('uid'))):
        return HttpResponseRedirect(reverse('index'))
    return render(request, 'quiz/index.html')



def quizlist(request):
    #open to students and above
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(1, uid):
        return HttpResponseRedirect(reverse('index'))
    #Get all quizzes that the user can access
    inner_qs = Class.objects.filter(user__pk=uid)
    quiz_list = Quiz.objects.filter(class_for__in=inner_qs)
    user = User.objects.get(pk=uid)

    context = {
        'uid': uid,
        'permissions': user.permissions,
        'class_list': inner_qs,
    }
    
    if user.permissions == 1:
        new_quiz_list = []
        taken_quiz_list = []
        #Ensure that nobody can take a quiz twice
        for quiz in quiz_list:
            #Ensure that students can only take published classes
            if quiz.published:
                if Record.objects.filter(user=user, quiz=quiz, finished=True).count() > 0:
                    taken_quiz_list.append(quiz)
                else:
                    new_quiz_list.append(quiz)
        context['new_quiz_list'] = new_quiz_list
        context['taken_quiz_list'] = taken_quiz_list
    else:
        context['unpublished_quiz_list'] = quiz_list.filter(published=False)
        context['published_quiz_list'] = quiz_list.filter(published=True).exclude(state="finished")
        context['finished_quiz_list'] = quiz_list.filter(published=True, state="finished")
        

    
    return render(request, 'quiz/quizlist.html', context)

#This is going to be the login pages for teachers, TAs, and students. 
def login(request):
    #Open to everyone
    if not handlePermissions(0, getUserId(request.session.get('uid'))):
        return HttpResponseRedirect(reverse('index'))

    #If user is already logged in, send them back to homepage
    if request.session.get('uid'):
        return HttpResponseRedirect(reverse("index"))

    #Message to render with site
    message = ""

    #If the user is attempting to log in
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            #This is where we check that the user is a person in the database
            u = None
            try:
                u = User.objects.get(name=form.cleaned_data['name'])#, password=form.cleaned_data['password'])
            except User.DoesNotExist:
                message = "That information is incorrect"

            #If the user entered valid login information
            if u != None:
                #This is where we set a cookie to say that the user is logged in
                request.session['uid'] = u.pk
                #return HttpResponseRedirect(reverse("index"))
                return HttpResponseRedirect(reverse("quizlist"))

        else:
            #If the form is not valid
            message = "You entered invalid information, somehow"
    else:
        #If someone isn't trying to login, just accessing the page
        if request.GET.get('message'):
            message=request.GET.get('message')
        form = LoginForm()
    #render the page
    return render(request, 'quiz/login.html', {'form': form, 'message': message})
    
def register(request):
    #Open to everyone, obviously
    uid = getUserId(request.session.get('uid'))
    #If user isn't already logged in
    if uid == -1:
        message = ""
        #If there is a message in the url (?message=Some+message), then send it to the page
        if request.GET.get('message'):
            message = request.GET.get('message')
        return render(request, 'quiz/register.html', {'message': message})
    else:
        return HttpResponseRedirect(reverse("index"))

def logout(request):
    #Open to everyone, obviously
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(0, uid):
        return HttpResponseRedirect(reverse('index'))
    #If the person is logged in
    if uid != -1:
        del request.session['uid']
    #Redirect to index page after this
    return HttpResponseRedirect(reverse("index"))

def edit_quiz(request, quiz_id):
    #Open to teachers and TAs
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(2, uid):
        return HttpResponseRedirect(reverse('index'))

    #TODO - Make sure that user can access this particular quiz

    #Get actual quiz
    quiz=Quiz.objects.get(pk=quiz_id)
    
    published = quiz.published

    if not published:

        #get quiz title
        title = quiz.name 

        #Get whether the quiz is published

        #Pass in a list of questions for the selected quiz, questionList
        #Format will be [[question object, [responses]], ...]
        questionList = []
        for q in Question.objects.filter(quiz=quiz):
            responses = Response.objects.filter(question=q)
            question = [q, responses]
            questionList = questionList + [question]

        context = {'title': title, 'questionList': questionList, 'uid': uid, 'quizId': quiz_id}

        return render(request, 'quiz/edit_quiz.html', context)
    else:
        return HttpResponseRedirect(reverse('view_quiz', args=[quiz_id]))
    
def take_quiz(request, quiz_id):
    #Open to students, teachers and TAs
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(1, uid):
        return HttpResponseRedirect(reverse('index'))

    #TODO - Make sure that user can access this particular quiz

    quiz=Quiz.objects.get(pk=quiz_id)

    #Get whether the quiz is published
    published = quiz.published

    if published:
        #get quiz title
        title = quiz.name 

        #Pass in a list of questions for the selected quiz, questionList
        #Format will be [[question object, [responses]], ...]
        questionList = []
        for q in Question.objects.filter(quiz=quiz):
            responses = Response.objects.filter(question=q)
            question = [q, responses]
            questionList = questionList + [question]

        context = {'title': title, 'questionList': questionList, 'uid': uid, 'quizId': quiz_id, 'published': published}

        return render(request, 'quiz/take_quiz.html', context)
    else:
        return HttpResponseRedirect(reverse("index"))

def view_quiz(request, quiz_id):
    #Open to students, teachers and TAs
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(1, uid):
        return HttpResponseRedirect(reverse('index'))

    #TODO - Make sure that user can access this particular quiz

    quiz=Quiz.objects.get(pk=quiz_id)

    #Get whether the quiz is published
    published = quiz.published

    if published:
        #get quiz title
        title = quiz.name 

        #Pass in a list of questions for the selected quiz, questionList
        #Format will be [[question object, [responses]], ...]
        questionList = []
        for q in Question.objects.filter(quiz=quiz):
            responses = Response.objects.filter(question=q)
            question = [q, responses]
            questionList = questionList + [question]


        context = {'title': title, 'questionList': questionList, 'uid': uid, 'quizId': quiz_id}

        #Pass in a record, if the user has one
        r = Record.objects.filter(quiz=quiz, user=User.objects.get(pk=uid))
        #If they do...
        if r.count == 1:
            context['record'] = r[0]

        return render(request, 'quiz/view_quiz.html', context)
    else:
        return HttpResponseRedirect(reverse("edit_quiz", args=[quiz_id]))

def edit_class(request, class_id):
    #Open to teachers
    uid = getUserId(request.session.get('uid'))
    if not handlePermissions(3, uid):
        return HttpResponseRedirect(reverse('index'))

    thisClass = Class.objects.get(pk=class_id)

    #TODO - ensure that teacher has access to this class

    studentsInClass = User.objects.filter(permissions=1, classes=thisClass)
    otherStudents = User.objects.filter(permissions=1).exclude(classes=thisClass)

    context = {
        'uid': uid,
        'thisClass': thisClass,
        'studentsInClass': studentsInClass,
        'otherStudents': otherStudents,
    } 

    return render(request, 'quiz/edit_class.html', context)
