from django.contrib import admin

from .models import User, Question, Quiz, Response, Class, Record

admin.site.register(User)
admin.site.register(Question)
admin.site.register(Quiz)
admin.site.register(Response)
admin.site.register(Class)
admin.site.register(Record)
