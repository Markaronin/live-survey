from django.db import models


class Response(models.Model):
    text = models.CharField(max_length=1024)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)

    def __str__(self):
        return self.text

class Question(models.Model):
    text = models.CharField(max_length=1024)
    correct_response = models.ForeignKey(Response, blank=True, null=True, on_delete=models.SET_NULL, related_name='+')
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    #This field is used during a live quiz to show which 
    #question to take next. This would allow for dynamic adding
    # or deleting of questions mid-quiz
    order = models.IntegerField(default=-1)

    def __str__(self):
        return self.text

class Quiz(models.Model):
    name = models.CharField(max_length=128)
    author = models.ForeignKey('User', on_delete=models.CASCADE)
    class_for = models.ForeignKey('Class', on_delete=models.CASCADE)
    published = models.BooleanField(default=False)
    start_time = models.DateTimeField(default=None, blank=True, null=True)
    state = models.CharField(max_length=128, default='none')
    current_question = models.IntegerField(default=-1)

    def __str__(self):
        return self.name

class User(models.Model):
    name = models.CharField(max_length=128)
    #password = models.CharField(max_length=128)
    permissions = models.IntegerField()
    #email = models.EmailField(max_length=128)
    classes = models.ManyToManyField('Class', blank=True)

    def __str__(self):
        return self.name

class Class(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name
    
    
class Record(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    responses = models.TextField(default="")
    finished = models.BooleanField(default=False)
