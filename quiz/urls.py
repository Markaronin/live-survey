from django.urls import path

from . import views

#app_name = 'quiz'
urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('quizlist', views.quizlist, name='quizlist'),
    path('edit_quiz/<int:quiz_id>/', views.edit_quiz, name='edit_quiz'),
    path('take_quiz/<int:quiz_id>/', views.take_quiz, name='take_quiz'),
    path('view_quiz/<int:quiz_id>/', views.view_quiz, name='view_quiz'),
    path('edit_class/<int:class_id>/', views.edit_class, name='edit_class'),
    #Forms
    path('form/register', views.register_form, name='register_form'),
    #Ajax
    path('ajax/validate_user_ajax', views.validate_user_ajax, name='validate_user_ajax'),
    path('ajax/modify_question_ajax', views.modify_question_ajax, name='modify_question_ajax'),
    path('ajax/modify_response_ajax', views.modify_response_ajax, name='modify_response_ajax'),
    path('ajax/publish_quiz_ajax', views.publish_quiz_ajax, name='publish_quiz_ajax'),
    path('ajax/modify_quiz_ajax', views.modify_quiz_ajax, name='modify_quiz_ajax'),
    path('ajax/modify_class_ajax', views.modify_class_ajax, name='modify_class_ajax'),
    path('ajax/get_quiz_info_ajax', views.get_quiz_info_ajax, name='get_quiz_info_ajax'),
]
