from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

from .models import User, Question, Quiz, Response, Class, Record

class QuizConsumer(WebsocketConsumer):
    users = {}
    #Function to get uid from channel_name
    def uid_from_channel_name(self):
        try:
            return self.users[self.room_group_name][next((i for i, v in enumerate(self.users[self.room_group_name]) if v[0] == self.channel_name), None)][1]
        except:
            return None
    def user_in_quiz(self, uid):
        try:
            return self.users[self.room_group_name][next((i for i, v in enumerate(self.users[self.room_group_name]) if v[1] == uid), None)][1]
        except:
            return None


    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['quiz_id']
        #Basically room name is quizId
        self.room_group_name = 'quiz_%s' % self.room_name

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
        try:
            #Remove user from self.users
            index = next((i for i, v in enumerate(self.users[self.room_group_name]) if v[0] == self.channel_name), None)
            disconnected_user = self.users[self.room_group_name].pop(index)
            #disconnected_user is variable that we could use eventually
        except:
            print("User left room " + self.room_group_name + " that was not in self.users")

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        data_type = text_data_json['type']

        if data_type == 'response':# If a user sends a response
            which = text_data_json['which']
            #Add data to the response for this user
            self.respond_to_question(which)
        elif data_type == 'start_quiz':#One-time thing for a teacher to start a quiz
            #Change the state of all users on the page to question
            message = {
                'type': 'change_state',
                'state': 'waiting',
            }
            #Send message to all users
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                message
            )
            quiz = Quiz.objects.get(pk=self.room_name)
            #Create responses for all connected users
            #Change quiz model
            #Ensure that this quiz starts at the first question
            quiz.current_question = -1
            quiz.save()
        elif data_type == 'join':
            uid = int(text_data_json['uid'])
            self.join_quiz(uid)
        elif data_type == 'start_question':
            message = {
                'type': 'change_state',
                'state': 'question',
            }

            quiz = Quiz.objects.get(pk=self.room_name)
            questions = Question.objects.filter(quiz=quiz).order_by('order', 'pk')
            quiz.current_question += 1
            quiz.state = "question"
            quiz.save()

            if quiz.current_question == len(questions) - 1:
                message['is_last'] = True
            else:
                message['is_last'] = False
                

            if quiz.current_question >= len(questions):
                print("Error - invalid current question")
                print("Current question index: " + str(quiz.current_question))
                print("Amount of questions: " + str(len(questions)))
                return
            question = questions[quiz.current_question]
            message['text'] = question.text
            message['responses'] = {}
            for r in Response.objects.filter(question=question):
                message['responses'][r.pk] = r.text
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                message
            )

        elif data_type == 'finish_question':
            quiz = Quiz.objects.get(pk=self.room_name)
            quiz.state = "answer"
            quiz.save()
            questions = Question.objects.filter(quiz=quiz).order_by('order', 'pk')
            message = {
                'type': 'change_state',
                'state': 'answer',
                'correct_response': questions[quiz.current_question].correct_response.pk,
                'response_amounts': text_data_json['response_amounts'],
            }
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                message
            )
        elif data_type == 'finish_quiz':
            quiz = Quiz.objects.get(pk=self.room_name)
            quiz.state = 'finished'
            quiz.save()
            rankings = {}
            #Construct list of user pks that aren't teachers
            u = [n[1] for n in self.users[self.room_group_name]]
            #Select all records that have users in this list and that have this quiz and that have users that aren't teachers
            records = Record.objects.filter(quiz=quiz, user__pk__in=u, user__permissions=1)
            correct_answers = [q.correct_response.pk for q in Question.objects.filter(quiz=quiz).order_by('order', 'pk')]
            #Tally up the score of each individual and sort the list
            for record in records:
                if record.responses != '':
                    score = 0
                    responses = set(list(map(int, record.responses.split(','))))
                    intersection = [value for value in correct_answers if value in responses]
                    rankings[record.user.name] = len(intersection)
                else:
                    print("Error - tried to finish quiz but user record had empty response field")
            #Sort the list
            rankings_order = sorted(rankings, key=rankings.get, reverse=True)
            #Send the list down to each user
            message = {
                'type': 'change_state',
                'state': 'finish',
                'rankings': rankings,
                'rankings_order': rankings_order,
            }
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                message
            )


    def change_state(self, event):
        state = event['state']
        message = {
            'type': 'change_state',
            'state': state,
        }
        if state == 'question':
            message['text'] = event['text']
            message['responses'] = event['responses']
            message['is_last'] = event['is_last']
            
            user = User.objects.get(pk=self.uid_from_channel_name())
            quiz = Quiz.objects.get(pk=self.room_name)
            r = Record.objects.get(user=user, quiz=quiz)
            #By default, add a -1 as the answer to the question. If the user responds, then we change -1 to their response
            if r.responses != '':
                resp = r.responses.split(',')
                resp.append('-1')
                r.responses = ','.join(resp)
            else:
                r.responses = '-1'
            r.save()
        elif state == 'answer':
            message['correct_response'] = event['correct_response']
            message['response_amounts'] = event['response_amounts']
        elif state == 'finish':
            user = User.objects.get(pk=self.uid_from_channel_name())
            quiz = Quiz.objects.get(pk=self.room_name)
            r = Record.objects.get(user=user, quiz=quiz)
            r.finished = True
            r.save()
            message['rankings'] = event['rankings']
            message['rankings_order'] = event['rankings_order']
        #Send the message to each person individually
        self.send(text_data=json.dumps(message))

    #Function that adds a response to a question
    def respond_to_question(self, which):
        uid = self.uid_from_channel_name()
        quiz = Quiz.objects.get(pk=self.room_name)
        r = Record.objects.get(user=User.objects.get(pk=uid), quiz=quiz)
        #Default responses value is the empty list
        resp = [which]
        #If it's not empty in the database, split it into an array
        if r.responses != "":
            resp = r.responses.split(',')
            #Stick the response at the end of the list
            resp[quiz.current_question] = which
        #Join it back together into a string and add it to the response field in the user record
        r.responses = ','.join(resp)
        r.save()
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name + "teacher",
            {
                "type": "student_response",
                "which": which,
            }
        )

        
    def join_quiz(self, uid):
        u = User.objects.filter(pk=uid)
        if u.count() != 1:
            print("Unauthorized user joining with uid " + str(uid))
        elif self.user_in_quiz(uid):
            print("User joining that is already in user list with uid " + str(uid))
        else:
            u = u[0]
            q = Quiz.objects.get(pk=self.room_name)
            if q.state == "finished": 
                print("Error - User trying to access finished quiz")
                return;
            # Join room group
            async_to_sync(self.channel_layer.group_add)(
                self.room_group_name,
                self.channel_name
            )
            if u.permissions == 3:
                async_to_sync(self.channel_layer.group_add)(
                    self.room_group_name + "teacher",
                    self.channel_name
                )
            if not self.room_group_name in self.users:
                self.users[self.room_group_name] = []
            self.users[self.room_group_name].append((self.channel_name, uid))
            
            #Check if user has record, if not then create one 
            r = Record.objects.filter(user=u, quiz=q)
            if r.count() == 0:
                #Create new record for this user
                r = Record(
                    user=u,
                    quiz=q,
                )
                if q.state == "none":
                    r.responses = ""
                elif q.state == "question" or q.state == "answer":
                    r.responses = ",".join(["-1"] * (q.current_question + 1))
                r.save()
            elif r.count() == 1:
                #Reset this user's record if they've already been here
                r = r[0]
                if q.state == "none":
                    r.responses = ""
                elif q.state == "question" or q.state == "answer":
                    r.responses = ",".join(["-1"] * (q.current_question + 1))
                r.finished = False
                r.save()
            else:
                print("There's a problem, houston. We've created more than one record for a single user")
            
            self.send(json.dumps({
                "type": "channel_join",
                "name": u.name,
                "permissions": u.permissions,
            }))

    def student_response(self, event):
        self.send(json.dumps({
            "type": "student_response",
            "which": event['which'],
        }))






