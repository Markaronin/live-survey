var uid = document.getElementById('uid').value;
var quizId = document.getElementById('quizId').value;
var isSelected = false;
var dictAnswers = new Object();

function addQuestion(){
    var question = prompt("Please enter the question text");
    //If the user didn't cancel or enter an empty prompt
    if(question != null && question != ""){
        //Do the ajax first
        
        $('body').data("tempId", -1);

        $.ajax({
            url: '/quiz/ajax/modify_question_ajax',
            method: 'POST',
            data: {
                'uid': uid,
                'type': 'add_question',
                'question': question,
                'quizId': quizId,
                'csrfmiddlewaretoken': csrf
            },
            dataType: 'json',
            async: false,
            success: function(dat){
                if(!dat.success){
                    alert("Something went wrong with the addQuestion ajax call");
                }
                $('body').data('tempId', dat.id);
            }
        });

        var newId = $('body').data('tempId');

        dictAnswers[newId] = false;

        //Render the new element on the page
        var newQuestion = document.createElement('li');
        newQuestion.setAttribute('id', 'q' + newId);

        //addQuestionId as key value pair to dictionary, false meaning no answer is selected

        var newQuestionText = document.createElement('span');
        newQuestionText.setAttribute('class', 'questionText');
        newQuestionText.setAttribute('which', newId);
        newQuestionText.textContent = question;
        
        var newQuestionEdit = document.createElement('input');
        newQuestionEdit.setAttribute('type', 'button');
        newQuestionEdit.setAttribute('class', 'editQuestion');
        newQuestionEdit.setAttribute('which', newId);
        newQuestionEdit.setAttribute('value', 'Edit');
        newQuestionEdit.onclick = function(){ editQuestion(this.getAttribute('which')); };

        var newQuestionDelete = document.createElement('input');
        newQuestionDelete.setAttribute('type', 'button');
        newQuestionDelete.setAttribute('class', 'deleteQuestion');
        newQuestionDelete.setAttribute('which', newId);
        newQuestionDelete.setAttribute('value', 'Delete');
        newQuestionDelete.onclick = function(){ deleteQuestion(this.getAttribute('which')); };

        var newQuestionResponses = document.createElement('ul');
        newQuestionResponses.setAttribute('class', 'responseList');
        newQuestionResponses.setAttribute('which', newId);
        var tempLi = document.createElement('li');

        var newQuestionAddResponse = document.createElement('input');
        newQuestionAddResponse.setAttribute('type', 'button');
        newQuestionAddResponse.setAttribute('class', 'addResponse');
        newQuestionAddResponse.setAttribute('value', 'Add a response');
        newQuestionAddResponse.setAttribute('which', newId);
        newQuestionAddResponse.onclick = function(){ addResponse(newId); };

        tempLi.appendChild(newQuestionAddResponse);
        newQuestionResponses.appendChild(tempLi);

        newQuestionResponses.appendChild(document.createTextNode(''));

        var questionList = document.getElementById("questionList");
        questionList.insertBefore(newQuestion, questionList.childNodes[questionList.childNodes.length-2]);

        newQuestion.appendChild(newQuestionText);
        newQuestion.appendChild(document.createTextNode(' '));
        newQuestion.appendChild(newQuestionEdit);
        newQuestion.appendChild(document.createTextNode(' '));
        newQuestion.appendChild(newQuestionDelete);
        newQuestion.appendChild(newQuestionResponses);
    }
}

function addResponse(id){
    var response = prompt("Please enter the response text");
    //If the user didn't cancel or enter an empty prompt
    if(response != null && response != ""){
        //Do an ajax call
        $('body').data("tempId", -1);

        $.ajax({
            url: '/quiz/ajax/modify_response_ajax',
            method: 'POST',
            data: {
                'uid': uid,
                'type': 'add_response',
                'response': response,
                'quizId': quizId,
                'questionId': id,
                'csrfmiddlewaretoken': csrf
            },
            dataType: 'json',
            async: false,
            success: function(dat){
                if(!dat.success){
                    alert("Something went wrong with the addResponse ajax call");
                }
                $('body').data('tempId', dat.id);
            }
        });

        var newId = $('body').data('tempId');

        var newResponse = document.createElement('li');
        newResponse.setAttribute('id', 'r' + newId);

        var newResponseText = document.createElement('span');
        newResponseText.setAttribute('class', 'responseText');
        newResponseText.setAttribute('which', newId);
        newResponseText.textContent = response;
        newResponse.appendChild(newResponseText);

        newResponse.appendChild(document.createTextNode(' '));

        var newResponseEdit = document.createElement('input');
        newResponseEdit.setAttribute('type', 'button');
        newResponseEdit.setAttribute('class', 'editResponse');
        newResponseEdit.setAttribute('which', newId);
        newResponseEdit.setAttribute('value', 'Edit');
        newResponseEdit.onclick = function(){ editResponse(this.getAttribute('which')); };
        newResponse.appendChild(newResponseEdit);

        newResponse.appendChild(document.createTextNode(' '));

        var newResponseDelete = document.createElement('input');
        newResponseDelete.setAttribute('type', 'button');
        newResponseDelete.setAttribute('class', 'deleteResponse');
        newResponseDelete.setAttribute('which', newId);
        newResponseDelete.setAttribute('value', 'Delete');
        newResponseDelete.onclick = function(){ deleteResponse(this.getAttribute('which')); };
        newResponse.appendChild(newResponseDelete);

        var selectAnswer = document.createElement('input')
        selectAnswer.setAttribute('type', 'button');
        selectAnswer.setAttribute('class', 'selectAnswer');
        selectAnswer.setAttribute('which', newId);
        selectAnswer.setAttribute('value', 'Answer');
        selectAnswer.setAttribute('questionId', id);
        selectAnswer.onclick = function(){
            choseAnswer(id, this.getAttribute('which'));
            alert(response + " is the current answer");
//            if(id in dictAnswers) {
//                dictAnswers[id] = this.getAttribute('which');
//                choseAnswer(id, this.getAttribute('which'));
//                alert(response + " is the current answer");
//
//            }
        };
        newResponse.appendChild(selectAnswer);

        var responseList = document.querySelector("ul.responseList[which='" + id + "']");
        responseList.lastElementChild.before(newResponse);
    }
}
//populate dictionary with the question ids, have a value = false...meaning no answer,
function choseAnswer(questionId, id) {
    $.ajax({
        url: '/quiz/ajax/modify_question_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'type': 'set_correct_response',
            'quizId': quizId,
            'questionId': questionId,
            'responseId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true,
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong in the choseAnswer ajax call");
            }
        }
    });

     //document.getElementById('r' + id);
}

function deleteQuestion(id){
    $.ajax({
        url: '/quiz/ajax/modify_question_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'type': 'delete_question',
            'quizId': quizId,
            'questionId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true, //This doesn't have to happen before the element is deleted
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong in the deleteQuestion ajax call");
            }
        }
    });
    document.getElementById('q' + id).remove();
}

function deleteResponse(id){
    $.ajax({
        url: '/quiz/ajax/modify_response_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'type': 'delete_response',
            'quizId': quizId,
            'responseId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true, //This doesn't have to happen before the element is deleted
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong in the deleteResponse ajax call");
            }
        }
    });
    document.getElementById('r' + id).remove();
}

function editQuestion(id){
    var newQuestion = prompt("Please enter the new question text");
    //If the user didn't cancel or enter an empty prompt
    if(newQuestion != null && newQuestion != ""){
        $.ajax({
            url: '/quiz/ajax/modify_question_ajax',
            method: 'POST',
            data: {
                'uid': uid,
                'type': 'edit_question',
                'quizId': quizId,
                'questionId': id,
                'newQuestion': newQuestion,
                'csrfmiddlewaretoken': csrf
            },
            dataType: 'json',
            async: true, //This doesn't have to happen before the element is edited
            success: function(dat){
                if(!dat.success){
                    alert("Something went wrong in the editQuestion ajax call");
                }
            }
        });
        var question = document.getElementById('q' + id);
        question.querySelector(".questionText").textContent = newQuestion;
    }
}

function editResponse(id){
    var newResponse = prompt("Please enter the new response text");
    //If the user didn't cancel or enter an empty prompt
    if(newResponse != null && newResponse != ""){
        $.ajax({
            url: '/quiz/ajax/modify_response_ajax',
            method: 'POST',
            data: {
                'uid': uid,
                'type': 'edit_response',
                'quizId': quizId,
                'responseId': id,
                'newResponse': newResponse,
                'csrfmiddlewaretoken': csrf
            },
            dataType: 'json',
            async: true, //This doesn't have to happen before the element is edited
            success: function(dat){
                if(!dat.success){
                    alert("Something went wrong in the editResponse ajax call");
                }
            }
        });
        var response = document.getElementById('r' + id);
        response.querySelector(".responseText").textContent = newResponse;
    }
}

function publish(){
    $.ajax({
        url: '/quiz/ajax/publish_quiz_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'quizId': quizId,
            'type': 'publish',
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true, 
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong in the publish ajax call");
            }
            window.location.replace('/quiz/view_quiz/' + quizId + '/');
        }
    });
    //TODO - make everything happen after this quiz is published
}

$(function(){
    document.querySelector('#addQuestion').onclick = function(){ addQuestion(); };

    $(".editQuestion").click(function(){
        editQuestion($(this).attr("which"));
    });

    $(".deleteQuestion").click(function(){
        deleteQuestion($(this).attr("which"));
    });

    $(".addResponse").click(function(){
        addResponse($(this).attr("which"));
    });

    $(".editResponse").click(function(){
        editResponse($(this).attr("which"));
    });

    $(".deleteResponse").click(function(){
        deleteResponse($(this).attr("which"));
    });

    $(".choseAnswer").click(function(){
        choseAnswer($(this).attr("questionId"), $(this).attr("which"));
    });
    
    var pub = document.getElementById("publish");
    pub.onclick = function(){ publish(); };
});
