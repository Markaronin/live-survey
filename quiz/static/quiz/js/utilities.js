//Adds a function to remove elements
Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

//Function to get cookie "cname". Not to be confused with getting sessions
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Get a csrf token that can be used in ajax calls
csrf = getCookie('csrftoken');
