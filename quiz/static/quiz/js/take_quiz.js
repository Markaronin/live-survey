//In case we ever want to use this
var uid = Number(document.getElementById('uid').value);
var quizId = Number(document.getElementById('quizId').value);

//This will be a dictionary/hash of all of the boxes
var boxes = {};
boxes['waiting'] = document.getElementById('waiting');
boxes['question'] = document.getElementById('question');
boxes['answer'] = document.getElementById('answer');
boxes['finish'] = document.getElementById('finish');
boxes['teacher_starting'] = document.getElementById('teacher_starting');
boxes['teacher_question'] = document.getElementById('teacher_question');
boxes['teacher_answer'] = document.getElementById('teacher_answer');
boxes['teacher_finish'] = document.getElementById('teacher_finish');
boxes['modify_quiz'] = document.getElementById('modify_quiz');

var socket;
var permissions = 0;

var screen = {
    state: "waiting",
    hideAll: function(){
        for(var box in boxes){
            boxes[box].style.display = "none";
        }
    },
    connectSocket: function(){
        socket = new WebSocket('ws://' + window.location.host + '/ws/take_quiz/' + quizId + '/');
        socket.onmessage = function(e){
            var data = JSON.parse(e.data);
            if(data['type'] === 'change_state'){
                if(data['state'] != 'waiting' || permissions == 1){
                    screen.state = data['state'];
                    screen.renderState(data);
                }
            }else if(data['type'] === 'channel_join'){
                console.log('Hello, ' + data['name'])
                permissions = data['permissions'];
                if(permissions == 2 || permissions == 3){
                    screen.state = "starting";
                }else if(permissions == 1){
                    screen.state = "waiting";
                }
                screen.renderState(data);
            }else if(data['type'] === 'student_response'){
                teacher_question.new_response(data);
            }
        };
        socket.onclose = function(e){
            console.error('Socket closed unexpectedly');
        };
        socket.onopen = function(e){
            socket.send(JSON.stringify({
                'type': 'join',
                'uid': uid,
            }));
        };
    },
    renderState: function(data={}){
        this.hideAll();
        if(permissions === 1){
            boxes[this.state].style.display = 'block';
            switch(this.state){
                case 'question':
                    question.render(data);
                    break;
                case 'waiting':
                    waiting.render(data);
                    break;
                case 'answer':
                    answer.render(data);
                    break;
                case 'finish':
                    finish.render(data);
                    break;
                default:
                    console.error('There was an invalid state passed in');
                    break;
            }
        }else if(permissions === 2 || permissions === 3){
            boxes['teacher_' + this.state].style.display = 'block';
            switch(this.state){
                case 'starting':
                    //This is a teacher-specific view to let them start the quiz
                    teacher_start.render(data);
                    break;
                case 'question':
                    teacher_question.how_many_responses = 0;
                    teacher_question.render(data);
                    break;
                case 'waiting':
                    //Do nothing, for now
                    break;
                case 'answer':
                    teacher_answer.render(data);
                    break;
                case 'finish':
                    teacher_finish.render(data);
                    break;
                default:
                    console.error('There was an invalid state passed in');
                    break;
            }
        }
    },
};

//Question will have the text for a label, and some questions that are labeled by ID but have the text A,B,C,D
var question = {
    text: "",
    responses: [],
    which: -1,
    is_last: false,
    render: function(data){
        //Fill out question with data from the server
        this.text = data['text'];
        this.responses = data['responses'];
        this.is_last = data['is_last'];

        //Remove previous content of the question box
        boxes['question'].innerHTML = "";

        //Create a box to hold the question title
        let t = document.createElement('div');
        t.setAttribute('class', 'questionTitleBox');
        t.textContent = this.text;
        boxes['question'].appendChild(t);

        //Create questions to fill up the box with
        for(var el in this.responses){
            let q = document.createElement('div');
            q.setAttribute('class', 'questionResponseBox');
            q.setAttribute('which', el);
            q.onclick = function(){ question.respond(this.getAttribute('which')); };
            q.textContent = this.responses[el];
            boxes['question'].appendChild(q);
        }
    },
    respond: function(which){
        this.which = Number(which);
        socket.send(JSON.stringify({
            'type': 'response',
            'which': which,
        }));
        screen.state = "waiting";
        screen.renderState();
    },
};

var waiting = {
    render: function(data){

    },
};

var answer = {
    render: function(data){
        let correct = data.correct_response === question.which;
        let answerText = document.getElementById('answerText');

        let successGif = document.getElementById('successGif');
        let failureGif = document.getElementById('failureGif');
        successGif.style.display = "none";
        failureGif.style.display = "none";
        if(correct){
            successGif.style.display = "block";
            answerText.textContent = "You got it right!"; 
        }else{
            failureGif.style.display = "block";
            answerText.textContent = "You got it wrong :("; 
        }
        let studentResponseChart = new ChartObj(question.responses, document.getElementById('studentResponseChart'));
        studentResponseChart.barsAmt = data.response_amounts;
        studentResponseChart.render();
    },
};

var finish = {
    render: function(data){
        let rankingsList = document.getElementById('studentFinishRankings');
        for(let i = 0; i < data.rankings_order.length; i++){
            let rank = document.createElement('li');
            rank.textContent = data.rankings_order[i] + ": " + String(data.rankings[data.rankings_order[i]]);
            rankingsList.appendChild(rank);
        }
    },
};

var teacher_start = {
    render: function(data){
        
    },
    start_quiz: function(){
        socket.send(JSON.stringify({
            'type': 'start_quiz',
        }));
        teacher_answer.next_question();
    },
};

var teacher_question = {
    is_last: false,
    how_many_responses: 0,
    responses: [1, 2, 3, 4],
    renderFinishButton: function(new_is_last){
        this.is_last = new_is_last;
        if(this.is_last){
            document.getElementById('nextQuestion').style.display = 'none';
            document.getElementById('finishQuiz').style.display = 'block';
        }else{
            document.getElementById('nextQuestion').style.display = 'block';
            document.getElementById('finishQuiz').style.display = 'none';
        }
    },
    render: function(data){
        this.renderFinishButton(data.is_last);
        document.getElementById('teacherQuestionText').textContent = data.text;
        document.getElementById('questionResponseAmount').textContent = this.how_many_responses;
        responseChart = new ChartObj(data.responses);
    },
    finish_question: function(data){
        socket.send(JSON.stringify({
            'type': 'finish_question',
            'response_amounts': responseChart.barsAmt,
        }));
        screen.state = 'answer';
        screen.renderState();
    },
    new_response: function(data){
        this.how_many_responses++;
        document.getElementById('questionResponseAmount').textContent = this.how_many_responses;
        responseChart.new_response(data.which);
    },
};

var teacher_answer = {
    render: function(data){

    },
    next_question: function(){ 
        socket.send(JSON.stringify({ 
            'type': 'start_question',
        }));
        screen.state = 'question';
    },
    finish: function(){
        socket.send(JSON.stringify({
            'type': 'finish_quiz',
        }));
    },
    modify_question: function(){

    }, 
};
var teacher_finish = {
    render: function(data){
        let rankingsList = document.getElementById('teacherFinishRankings');
        for(let i = 0; i < data.rankings_order.length; i++){
            let rank = document.createElement('li');
            rank.textContent = data.rankings_order[i] + ": " + String(data.rankings[data.rankings_order[i]]);
            rankingsList.appendChild(rank);
        }
    },
};

var modify_quiz = {
    render: function(){
        document.getElementById('questionList').innerHTML = "";
        fetch('/quiz/ajax/get_quiz_info_ajax', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                quiz_id: quizId,
            }),
            headers: { 
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "X-CSRFToken": csrf,
            },
        })
        .then(response => response.json())
        .then(data => {
            let questions = data.question_list;
            let questionList = document.getElementById('questionList');
            questionList.setAttribute('id', 'questionList');
            for(let i = 0; i < questions.length; i++){
                let question = newQuestionElement(questions[i][0], questions[i][1], questions[i][2], questions[i][3]);
                questionList.appendChild(question);
            }
            let addQuestionLi = document.createElement('li');
            let addQuestion = document.createElement('input');
            addQuestion.setAttribute('class', 'addQuestion');
            addQuestion.setAttribute('type', 'button');
            addQuestion.setAttribute('which', quizId);
            addQuestion.setAttribute('value', 'Add a Question');
            addQuestion.onclick = function(){ add_question(this.getAttribute('which')); };
            addQuestionLi.appendChild(addQuestion);
            questionList.appendChild(addQuestionLi);

            boxes['modify_quiz'].style.display = 'block';
        });
    },
    hide: function(){
        //Code that makes sure that we can't finish a quiz when there are still questions to answer
        if(document.getElementById('questionList').childElementCount > 1){
            teacher_question.renderFinishButton(false);
        }else if(document.getElementById('questionList').childElementCount === 1){
            teacher_question.renderFinishButton(true);
        }else{
            console.error('There were zero children of the questionList element when hiding modify_quiz');
        }
        boxes['modify_quiz'].style.display = 'none';
    },
};

//Object that can represent a chart
function ChartObj(responses, chart=document.getElementById('responseChart')){
    this.ch = chart;
    this.amount = Object.keys(responses).length;
    this.bars = [];
    this.barsAmt = [];
    this.barsPk = [];
    //Initialize the chart style
    this.ch.innerHTML = "";
    this.ch.style.height = "30%";
    this.ch.style.width = "100%";
    for(let i = 0; i < this.amount; i++){
        let newBar = document.createElement('div');
        newBar.setAttribute('which', Object.keys(responses)[i]);
        newBar.setAttribute('value', responses[Object.keys(responses)[i]]);
        //newBar.style.border = "1px solid black";
        newBar.style['background-color'] = "rgb(128, 128, " + ((255 / (this.amount - 1)) * i) + ")";
        newBar.style.position = "absolute";
        newBar.style.top = (this.ch.offsetTop + this.ch.clientHeight) + "px";
        newBar.style.left = String(this.ch.offsetLeft + ((this.ch.offsetWidth / this.amount) * i)) + "px";
        newBar.style.width = String(this.ch.offsetWidth / this.amount) + "px";
        newBar.style.height = "0px";
        newBar.style['text-align'] = "center";
        newBar.innerHTML = "<h4>" + newBar.getAttribute('value') + ": 0</h4>";
        this.ch.appendChild(newBar);
        this.bars.push(newBar);
        this.barsAmt.push(0);
        this.barsPk.push(Number(Object.keys(responses)[i]));
    }
    this.maxHeight = function(){
        let maxH = 0;
        for(let i = 0; i < this.amount; i++){
            if(this.barsAmt[i] > maxH){
                maxH = this.barsAmt[i];
            }
        }
        return maxH;
    };
    this.render = function(){
        let maxH = this.maxHeight();
        for(let i = 0; i < this.amount; i++){
            if(this.barsAmt[i] !== 0){
                this.bars[i].style.height = (this.ch.clientHeight / (maxH / this.barsAmt[i])) + "px";
                this.bars[i].innerHTML = "<h4>" + this.bars[i].getAttribute('value') + ": " + this.barsAmt[i] + "</h4>";
            }
            this.bars[i].style.top = (this.ch.offsetTop + this.ch.clientHeight - this.bars[i].clientHeight) + "px";
        }
    };
    this.new_response = function(which){
        let index = this.barsPk.indexOf(Number(which));
        this.barsAmt[index]++;
        this.render();
    };
};

var responseChart;

//Stuff to do when the page loads
screen.connectSocket();

document.getElementById('startQuiz').addEventListener('click', function(){
    teacher_start.start_quiz();
    document.getElementById('wii').play();
});
document.getElementById('wii').onended = function(){
    document.getElementById('wii').play();
};

document.getElementById('finishQuestion').onclick = teacher_question.finish_question;
document.getElementById('nextQuestion').onclick = teacher_answer.next_question;
document.getElementById('finishQuiz').onclick = teacher_answer.finish;
let modifyQuizButtons = document.querySelectorAll('.modifyQuizButton');
for(let i = 0; i < modifyQuizButtons.length; i++){
    modifyQuizButtons[i].onclick = modify_quiz.render;
}
document.getElementById('finishModify').onclick = modify_quiz.hide;

function add_question(which){
    let question = prompt('What should the question text be?');
    if(question != null && question != ""){
        fetch('/quiz/ajax/modify_question_ajax', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                type: 'add_question',
                quizId: quizId,
                question: question,
            }),
            headers: { 
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "X-CSRFToken": csrf,
            },
        })
        .then(response => response.json())
        .then(data => {
            if(data.success){
                let newQuestion = newQuestionElement(data.id, question, null, []);
                document.getElementById('questionList').lastChild.before(newQuestion);
                checkFinish();
            }else{
                console.error('Ajax call failed in add_question');
            }
        });
    }
}

function edit_question(which){
    let question = prompt('What should the question text be?');
    if(question != null && question != ""){
        fetch('/quiz/ajax/modify_question_ajax', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                type: 'edit_question',
                quizId: quizId,
                newQuestion: question,
                questionId: which,
            }),
            headers: { 
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "X-CSRFToken": csrf,
            },
        })
        .then(response => response.json())
        .then(data => {
            if(data.success){
                let thisQuestion = document.querySelector('.questionText[which="' + which + '"]')
                thisQuestion.textContent = question;
                checkFinish();
            }else{
                console.error('Ajax call failed in edit_question');
            }
        });
    }
}

function delete_question(which){
    fetch('/quiz/ajax/modify_question_ajax', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            type: 'delete_question',
            questionId: which,
        }),
        headers: { 
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
        },
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            let thisQuestion = document.querySelector('.question[which="' + which + '"]')
            thisQuestion.remove();
            checkFinish();
        }else{
            console.error('Ajax call failed in delete_question');
        }
    });
}

function drag_question(event, which){
    event.dataTransfer.setData("text/plain", which);
}

function drag_over_question(event, which){
    event.preventDefault();
    let question = document.querySelector('.question[which="' + which + '"]');
    question.style['background-color'] = "green";
}

function drag_leave_question(event, which){
    event.preventDefault();
    let question = document.querySelector('.question[which="' + which + '"]');
    question.style['background-color'] = "";
}

function drop_question(event, which){
    event.preventDefault();
    let question = document.querySelector('.question[which="' + which + '"]');
    question.style['background-color'] = "";
    let whichDragged = event.dataTransfer.getData('text/plain');
    fetch('/quiz/ajax/modify_question_ajax', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            type: 'swap_question',
            quizId: quizId,
            firstQuestion: which,
            secondQuestion: whichDragged,
        }),
        headers: { 
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
        },
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            let q1 = document.querySelector('.question[which="' + which + '"]')
            let q2 = document.querySelector('.question[which="' + whichDragged + '"]')
            let t = q1.parentNode.insertBefore(document.createTextNode(''), q1);
            q2.parentNode.insertBefore(q1, q2);
            t.parentNode.insertBefore(q2, t);
            t.parentNode.removeChild(t);
            checkFinish();
        }else{
            console.error('Ajax call failed in drop_question');
        }
    });
}

function add_response(which){
    let response = prompt('What should the response text be?');
    if(response != null && response != ""){
        fetch('/quiz/ajax/modify_response_ajax', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                type: 'add_response',
                response: response,
                questionId: which,
            }),
            headers: { 
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "X-CSRFToken": csrf,
            },
        })
        .then(response => response.json())
        .then(data => {
            if(data.success){
                let newResponse = newResponseElement(data.id, which, response, false);
                document.querySelector('.responseList[which="' + which + '"]').lastChild.before(newResponse);
                checkFinish();
            }else{
                console.error('Ajax call failed in add_response');
            }
        });
    }
}

function edit_response(which){
    let response = prompt('What should the response text be?');
    if(response != null && response != ""){
        fetch('/quiz/ajax/modify_response_ajax', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                type: 'edit_response',
                newResponse: response,
                responseId: which,
            }),
            headers: { 
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "X-CSRFToken": csrf,
            },
        })
        .then(response => response.json())
        .then(data => {
            if(data.success){
                let thisResponse = document.querySelector('.responseText[which="' + which + '"]');
                thisResponse.textContent = response;
                checkFinish();
            }else{
                console.error('Ajax call failed in edit_response');
            }
        });
    }
}

function delete_response(which){
    fetch('/quiz/ajax/modify_response_ajax', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            type: 'delete_response',
            responseId: which,
        }),
        headers: { 
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
        },
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            let thisResponse = document.querySelector('.response[which="' + which + '"]');
            thisResponse.remove();
            checkFinish();
        }else{
            console.error('Ajax call failed in delete_response');
        }
    });
}

function set_correct_response(whichQuestion, whichResponse){
    fetch('/quiz/ajax/modify_question_ajax', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            type: 'set_correct_response',
            questionId: whichQuestion,
            responseId: whichResponse,
        }),
        headers: { 
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
        },
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            let thisResponse = document.querySelector('.response[which="' + whichResponse + '"]');
            let thisResponseCorrect = thisResponse.querySelector('.correctResponse');

            let currentResponseCorrect = document.querySelector('.correctResponseSpan[whichQuestion="' + whichQuestion + '"]')
            if(currentResponseCorrect){
                let currentResponse = currentResponseCorrect.parentElement;
                currentResponseCorrect.remove();
                let correctResponseButton = document.createElement('input');
                correctResponseButton.setAttribute('type', 'button');
                correctResponseButton.setAttribute('class', 'correctResponse');
                correctResponseButton.setAttribute('whichQuestion', whichQuestion);
                correctResponseButton.setAttribute('whichResponse', currentResponse.getAttribute('which'));
                correctResponseButton.setAttribute('value', 'Set Correct Response');
                correctResponseButton.onclick = function(){ set_correct_response(this.getAttribute('whichQuestion'), this.getAttribute('whichResponse')); };
                currentResponse.appendChild(correctResponseButton);
            }

            thisResponseCorrect.remove();
            let correctResponseSpan = document.createElement('span');
            correctResponseSpan.setAttribute('class', 'correctResponseSpan');
            correctResponseSpan.setAttribute('whichQuestion', whichQuestion);
            correctResponseSpan.setAttribute('whichResponse', whichResponse);
            correctResponseSpan.textContent = "This is the correct response";
            thisResponse.appendChild(correctResponseSpan);
            
            checkFinish();
        }else{
            console.error('Ajax call failed in set_correct_response');
        }
    });
}

//Functions to disable the finish_modifying_quiz button until everything is okay
function checkFinish(){
    //Loop through all of the questions and make sure that they have responses
    let okay = true;
    let questions = document.querySelectorAll('.question');
    for(let i = 0; i < questions.length; i++){
        let responses = questions[i].childNodes[3];
        //If there is at least one response and the question has a correct reponse
        if(!(responses.childElementCount > 1 && responses.querySelector('.correctResponseSpan'))){
            okay = false;
        }
    }
    if(okay){
        document.getElementById('finishModify').disabled = false;
    }else{
        document.getElementById('finishModify').disabled = true;
    }
}

//Functions to make creating elements easier
function newResponseElement(whichResponse, whichQuestion, newResponseText, isCorrect){
    let response = document.createElement('li');
    response.setAttribute('class', 'response');
    response.setAttribute('which', whichResponse);

    let responseText = document.createElement('span');
    responseText.textContent = newResponseText;
    responseText.setAttribute('class', 'responseText');
    responseText.setAttribute('which', whichResponse);
    response.appendChild(responseText);

    let editResponse = document.createElement('input');
    editResponse.setAttribute('class', 'editResponse');
    editResponse.setAttribute('type', 'button');
    editResponse.setAttribute('which', whichResponse);
    editResponse.setAttribute('value', 'Edit');
    editResponse.onclick = function(){ edit_response(this.getAttribute('which')); };
    response.appendChild(editResponse);

    let deleteResponse = document.createElement('input');
    deleteResponse.setAttribute('class', 'deleteResponse');
    deleteResponse.setAttribute('type', 'button');
    deleteResponse.setAttribute('which', whichResponse);
    deleteResponse.setAttribute('value', 'Delete');
    deleteResponse.onclick = function(){ delete_response(this.getAttribute('which')); };
    response.appendChild(deleteResponse);

    //If this is the correct response
    if(isCorrect){
        let correctResponseSpan = document.createElement('span');
        correctResponseSpan.setAttribute('class', 'correctResponseSpan');
        correctResponseSpan.setAttribute('whichQuestion', whichQuestion);
        correctResponseSpan.setAttribute('whichResponse', whichResponse);
        correctResponseSpan.textContent = "This is the correct response";
        response.appendChild(correctResponseSpan);
    }else{
        let correctResponseButton = document.createElement('input');
        correctResponseButton.setAttribute('type', 'button');
        correctResponseButton.setAttribute('class', 'correctResponse');
        correctResponseButton.setAttribute('whichQuestion', whichQuestion);
        correctResponseButton.setAttribute('whichResponse', whichResponse);
        correctResponseButton.setAttribute('value', 'Set Correct Response');
        correctResponseButton.onclick = function(){ set_correct_response(this.getAttribute('whichQuestion'), this.getAttribute('whichResponse')); };
        response.appendChild(correctResponseButton);
    }
    return response;
}
function newQuestionElement(whichQuestion, newQuestionText, correctResponse, responses){
    let question = document.createElement('li');
    question.setAttribute('class', 'question');
    question.setAttribute('draggable', 'true');
    question.ondragstart = function(){ drag_question(event, this.getAttribute('which')); };
    question.ondragover = function(){ drag_over_question(event, this.getAttribute('which')); };
    question.ondragleave = function(){ drag_leave_question(event, this.getAttribute('which')); };
    question.ondrop = function(){ drop_question(event, this.getAttribute('which')); };
    question.setAttribute('which', whichQuestion);

    let questionText = document.createElement('span');
    questionText.setAttribute('class', 'questionText');
    questionText.setAttribute('which', whichQuestion);
    questionText.textContent = newQuestionText;
    question.appendChild(questionText);

    let editQuestion = document.createElement('input');
    editQuestion.setAttribute('class', 'editQuestion');
    editQuestion.setAttribute('type', 'button');
    editQuestion.setAttribute('which', whichQuestion);
    editQuestion.setAttribute('value', 'Edit');
    editQuestion.onclick = function(){ edit_question(this.getAttribute('which')); };
    question.appendChild(editQuestion);

    let deleteQuestion = document.createElement('input');
    deleteQuestion.setAttribute('class', 'deleteQuestion');
    deleteQuestion.setAttribute('type', 'button');
    deleteQuestion.setAttribute('which', whichQuestion);
    deleteQuestion.setAttribute('value', 'Delete');
    deleteQuestion.onclick = function(){ delete_question(this.getAttribute('which')); };
    question.appendChild(deleteQuestion);

    let responseList = document.createElement('ul');
    responseList.setAttribute('class', 'responseList');
    responseList.setAttribute('which', whichQuestion);

    for(let j = 0; j < responses.length; j++){
        let response = newResponseElement(responses[j][0], whichQuestion, responses[j][1], (correctResponse === responses[j][0]));
        responseList.appendChild(response);
    }

    let addResponseLi = document.createElement('li');
    let addResponse = document.createElement('input');
    addResponse.setAttribute('class', 'addResponse');
    addResponse.setAttribute('type', 'button');
    addResponse.setAttribute('which', whichQuestion);
    addResponse.setAttribute('value', 'Add a Response');
    addResponse.onclick = function(){ add_response(this.getAttribute('which')); };
    addResponseLi.appendChild(addResponse);
    responseList.appendChild(addResponseLi);

    question.appendChild(responseList);
    return question;
}
