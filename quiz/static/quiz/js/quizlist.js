var uid = document.querySelector('#uid').value;
var unpubQuizList = document.getElementById('unpublishedQuizList');
var pubQuizList = document.getElementById('publishedQuizList');
var addQuizBox = document.getElementById('published_quiz_list');

function copyQuiz(id){
    $.ajax({
        url: '/quiz/ajax/modify_quiz_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'type': 'copy_quiz',
            'quizId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: false,
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong with the copyQuiz ajax call");
            }
            $('body').data('tempId', dat.id);
            $('body').data('tempName', dat.name);
            $('body').data('tempUrl', dat.url);

        }
    });


    var newId = $('body').data('tempId');
    
    var newListElement = document.createElement('li');
    
    var quizLink = document.createElement('a');
    quizLink.setAttribute('href', $('body').data('tempUrl'));
    quizLink.setAttribute('class', 'quizText');
    quizLink.setAttribute('which', newId);
    quizLink.textContent = $('body').data('tempName');

    var quizCopy = document.createElement('input');
    quizCopy.setAttribute('type', 'button');
    quizCopy.setAttribute('class', 'copyQuiz');
    quizCopy.setAttribute('which', newId);
    quizCopy.setAttribute('value', 'Copy');
    quizCopy.onclick = function(){
        copyQuiz(this.getAttribute('which'));
    };

    var quizDelete = document.createElement('input');
    quizDelete.setAttribute('type', 'button');
    quizDelete.setAttribute('class', 'deleteQuiz');
    quizDelete.setAttribute('which', newId);
    quizDelete.setAttribute('value', 'Delete');
    quizDelete.onclick = function(){
        deleteQuiz(this.getAttribute('which'));
    };

    newListElement.appendChild(quizLink);
    newListElement.appendChild(document.createTextNode(' '));
    newListElement.appendChild(quizCopy);
    newListElement.appendChild(quizDelete);

    if(!unpubQuizList){
        let unpubQuizListTitle = document.createElement('h3');
        unpubQuizListTitle.textContent = "Unpublished Quizzes";
        document.getElementById('uid').after(unpubQuizListTitle);
        unpubQuizList = document.createElement('ol');
        unpubQuizList.setAttribute('class', 'rounded-list');
        unpubQuizList.setAttribute('id', 'unpublishedQuizList');
        unpubQuizListTitle.after(unpubQuizList);
    }
    unpubQuizList.appendChild(newListElement);
}

function deleteQuiz(id){
    $.ajax({
        url: '/quiz/ajax/modify_quiz_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'type': 'delete_quiz',
            'quizId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        success: function(dat){
            if(!dat.success){
                alert("Something went wrong with the deleteQuiz ajax call");
            }else{
                document.querySelector(".deleteQuiz[which='" + id + "']").parentElement.remove();
            }
        }
    });
}

function addQuiz(id){

    var name = document.getElementById("quiz_name").value;
//    var class_for = document.getElementById("class_for").value; //use when we feel like
    var class_for = 1;

    if(name != null && name != "")  {
        $('body').data("tempId", -1);

        $.ajax({
            url: '/quiz/ajax/modify_quiz_ajax',
            method: 'POST',
            data: {
                'uid': uid,
                'type': 'add_quiz',
                'name': name,
                'class_for': class_for,
                'quizId': id, //get this from the page...check it out on quizlist.html
                'csrfmiddlewaretoken': csrf
            },
            dataType: 'json',
            async: false,
            success: function(dat){
                if(!dat.success){
                    alert("Something went wrong with the addQuiz ajax call");
                }

                $('body').data('tempId', dat.id);
                var newId = $('body').data('tempId');
                location.href = 'edit_quiz/'+ newId;



            }
        });
    }

    var newId = $('body').data('tempId');

    var newListElement = document.createElement('li');

    var newNameText = document.createTextNode(name);
    ///newNameText.textContent = name
    //newNameText.setAttribute('data-content', name)
    console.log("new name text", newNameText)
    newListElement.appendChild(newNameText);
    console.log("new list element", newListElement);

    var editQuiz = document.createElement('input');
    editQuiz.setAttribute('type', 'button');
    editQuiz.setAttribute('class', 'editQuiz');
    editQuiz.setAttribute('which', newId);
    editQuiz.setAttribute('value', 'Edit')

    var deleteQuiz = document.createElement('input');
    deleteQuiz.setAttribute('type', 'button');
    deleteQuiz.setAttribute('class', 'deleteQuiz');
    deleteQuiz.setAttribute('which', newId);
    deleteQuiz.setAttribute('value', 'Delete')

    addQuizBox.appendChild(newListElement);
}

$(function(){
    var copyQuizList = document.querySelectorAll('.copyQuiz')
    for(var i = 0; i < copyQuizList.length; i++){
        copyQuizList[i].onclick = function(event){
            copyQuiz(event.target.getAttribute('which'));
        };
    }
    var deleteQuizList = document.querySelectorAll('.deleteQuiz')
    for(var i = 0; i < deleteQuizList.length; i++){
        deleteQuizList[i].onclick = function(event){
            deleteQuiz(event.target.getAttribute('which'));
        };
    }
    var addQuizButton = document.getElementById('addQuiz');
    if(addQuizButton)
        addQuizButton.onclick = function(){ addQuiz() };

});
