uid = document.getElementById('uid').value;
classId = document.getElementById('classId').value;

function addStudent(id, name){
    document.querySelector('.addStudent[which="' + id + '"]').setAttribute('disabled', '');
    $.ajax({
        url: '/quiz/ajax/modify_class_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'classId': classId,
            'type': 'add_student',
            'studentId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true,
        success: function(dat){
            //Remove the old student
            if(!dat.success){
                console.log('something went wrong in removeStudent()');
            }
            document.querySelector('.student[which="' + id + '"]').remove();


            var studentListElement = document.createElement('li');
            studentListElement.setAttribute('class', 'student');
            studentListElement.setAttribute('which', id);

            var studentText = document.createElement('span');
            studentText.setAttribute('class', 'studentName');
            studentText.setAttribute('which', id);
            studentText.textContent = name;

            var studentRemove = document.createElement('input');
            studentRemove.setAttribute('type', 'button');
            studentRemove.setAttribute('class', 'removeStudent');
            studentRemove.setAttribute('which', id);
            studentRemove.setAttribute('value', 'Remove');
            studentRemove.onclick = function(event){
                let thisId = this.getAttribute('which');
                removeStudent(
                    thisId,
                    document.querySelector('.studentName[which="' + thisId + '"]').textContent 
                );
            };

            studentListElement.appendChild(studentText);
            studentListElement.appendChild(document.createTextNode(' '));
            studentListElement.appendChild(studentRemove);
            //Go through the things and add this to the studentsInClass list
            
            //Temp
            var studentsInClass = document.getElementById('studentsInClass');
            studentsInClass.appendChild(studentListElement);
        }
    });
}

function removeStudent(id, name){
    document.querySelector('.removeStudent[which="' + id + '"]').setAttribute('disabled', '');
    $.ajax({
        url: '/quiz/ajax/modify_class_ajax',
        method: 'POST',
        data: {
            'uid': uid,
            'classId': classId,
            'type': 'remove_student',
            'studentId': id,
            'csrfmiddlewaretoken': csrf
        },
        dataType: 'json',
        async: true,
        success: function(dat){
            //Remove the old student
            if(!dat.success){
                console.log('something went wrong in removeStudent()');
            }
            document.querySelector('.student[which="' + id + '"]').remove();


            var studentListElement = document.createElement('li');
            studentListElement.setAttribute('class', 'student');
            studentListElement.setAttribute('which', id);

            var studentText = document.createElement('span');
            studentText.setAttribute('class', 'studentName');
            studentText.setAttribute('which', id);
            studentText.textContent = name;

            var studentAdd = document.createElement('input');
            studentAdd.setAttribute('type', 'button');
            studentAdd.setAttribute('class', 'addStudent');
            studentAdd.setAttribute('which', id);
            studentAdd.setAttribute('value', 'Add');
            studentAdd.onclick = function(event){
                let thisId = this.getAttribute('which');
                addStudent(
                    thisId,
                    document.querySelector('.studentName[which="' + thisId + '"]').textContent 
                );
            };

            studentListElement.appendChild(studentText);
            studentListElement.appendChild(document.createTextNode(' '));
            studentListElement.appendChild(studentAdd);
            //Go through the things and add this to the studentsInClass list
            
            //Temp
            var otherStudents = document.getElementById('otherStudents');
            otherStudents.appendChild(studentListElement);
        }
    });
}

$(function(){
    $('.addStudent').click(function(){
        addStudent(
            $(this).attr('which'),
            document.querySelector('.studentName[which="' + $(this).attr('which') + '"]').textContent
        );
    });
    $('.removeStudent').click(function(){
        removeStudent(
            $(this).attr('which'),
            document.querySelector('.studentName[which="' + $(this).attr('which') + '"]').textContent
        );
    });
});
