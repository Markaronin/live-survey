from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/take_quiz/(?P<quiz_id>[0-9]+)/$', consumers.QuizConsumer),
]
