#Template


##Jefferson
10/10/2018 - 2 hours
10/12/2018 - 2 hours
10/18/2018 - 1 hour
10/22/2018 - 2 hours
10/23/2018 - 1 hour
10/24/2018 - 4 hours

10/29/2018 - 2 hours
11/02/2018 - 4 hours
11/05/2018 - 3 hours


##Elise
10/10/2018: 0.75 hrs (in class)
10/12/2018: 2 hours (UML diagram)
10/15/2018: 1 hour (Django)
10/20/2018: 2 hours

##Chase
(Sprint 1)
10/10/2018: 0.75 hrs
10/12/2018: 2.00 hrs
10/21/2018: 2.00 hrs (Django tutorial)
10/22/2018: 1.00 hrs (Adding basic quizlist)
10/24/2018: 2.00 hrs

(Sprint 2)
10/29/2018: 1.00 hrs (Create a record class)
10/31/2018: 1.00 hrs (Add a one-to-many response foreign key field)
